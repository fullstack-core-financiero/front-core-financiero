import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DefaultLayoutComponent } from './containers';
import { Page404Component } from './views/pages/page404/page404.component';
import { Page500Component } from './views/pages/page500/page500.component';
import { LoginComponent } from './views/pages/login/login.component';
import { RegisterComponent } from './views/pages/register/register.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '404',
    component: Page404Component,
    data: { title: 'Page 404' }
  },
  {
    path: '500',
    component: Page500Component,
    data: { title: 'Page 500' }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login Page' }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./views/dashboard/dashboard.module').then((m) => m.DashboardModule)
      },
      { path: 'persona-natural',
        loadChildren: () =>
          import('./views/core-financiero/persona-natural/persona-natural.module').then(m => m.PersonaNaturalModule)
      },
      { path: 'persona-juridica',
        loadChildren: () => import('./views/core-financiero/persona-juridica/persona-juridica.module').then(m => m.PersonaJuridicaModule)
      }
    ]
  },
  {
    path: '**', component: Page404Component
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'top',
      anchorScrolling: 'enabled',
      initialNavigation: 'enabledBlocking'
      // relativeLinkResolution: 'legacy'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
