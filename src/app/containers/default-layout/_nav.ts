import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    iconComponent: { name: 'cil-speedometer' }
  },
  {
    name: 'Widgets',
    url: '/widgets',
    iconComponent: { name: 'cil-calculator' }
  },
  {
    name: 'Core-Financiero',
    url:'/core-financiero',
    iconComponent: { name: 'cil-calculator' }
  }
];
