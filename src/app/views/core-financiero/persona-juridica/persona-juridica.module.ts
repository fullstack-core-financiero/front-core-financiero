import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonaJuridicaRoutingModule } from './persona-juridica-routing.module';
import { PersonaJuridicaComponent } from './persona-juridica.component';


@NgModule({
  declarations: [
    PersonaJuridicaComponent
  ],
  imports: [
    CommonModule,
    PersonaJuridicaRoutingModule
  ]
})
export class PersonaJuridicaModule { }
