import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonaJuridicaComponent } from './persona-juridica.component';

const routes: Routes = [{ path: '', component: PersonaJuridicaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonaJuridicaRoutingModule { }
