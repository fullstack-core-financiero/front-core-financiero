import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonaNaturalComponent } from './persona-natural.component';

const routes: Routes = [{ path: '', component: PersonaNaturalComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonaNaturalRoutingModule { }
