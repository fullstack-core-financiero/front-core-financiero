import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonaNaturalRoutingModule } from './persona-natural-routing.module';
import { PersonaNaturalComponent } from './persona-natural.component';


@NgModule({
  declarations: [
    PersonaNaturalComponent
  ],
  imports: [
    CommonModule,
    PersonaNaturalRoutingModule
  ]
})
export class PersonaNaturalModule { }
